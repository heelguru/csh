*This dofile links all the dofiles needed to 
*do geocode-halls.do // This do file geocodes private and university halls from postcodes
*do geocode-unihalls.do // This do file geocodes halls solely owned by universities
*do halls-treatment-control.do // this aggregates the halls by neighbourhoods
*do halls-donut-treatment-control.do // this aggregates halls data for donuts
do cases-analysis-boot.do // main analysis including TWFE no stagger start estimates
do cases-analysis-donut-boot.do // Donut analysis
do cases-analysis-unihalls.do // Performs estimation using Huntington-Klein's did wrapper
do cases-plots-boot.do // This dofile takes all estimates and plots them for main analysis
do cases-plots-donuts-boot.do // This dofile takes estimates from donut analysis and plots them
do cases-plots-unihalls.do // This dofile plots Callaway and Sant'Anna estimates