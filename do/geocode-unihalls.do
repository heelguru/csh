clear all
local datar ../data/raw
local datad ../data/derived
import excel using `datar'/hall-data.xlsx,sheet(uni) first clear case(lower)
gen num=1
frame copy default startdates
frame change startdates
collapse (firstnm) start ,by(uni)
tostring startd,replace
gen start=date(startd,"YMD")
drop startdates
format start %td
frame change default
keep uni postcode halls num
frame copy default halls
import excel using `datar'/hall-data.xlsx,sheet(private) first clear case(lower) cellra(a1:d105)
gen num=1
frameappend halls,drop
frame copy default halls
import excel using `datar'/hall-eva.xlsx, first clear cellra(a1:c278) case(lower)
frameappend halls,drop
replace postcode=trim(itrim(postcode))
strkeep postcode,keep("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ") replace
strkeep halls,keep("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ") replace
replace halls=trim(itrim(halls))
sort postcode halls num
duplicates tag postcode halls,g(tag)
gen sruc=strpos(halls,"SRUC")
replace uni="SRUC" if sruc==1
gen todrop=0
replace todrop=1 if halls=="Keiller Court" & mi(provider)
replace todrop=1 if halls=="Brae House" & mi(provider)
replace todrop=1 if halls=="St Mungos" & mi(provider)
replace todrop=1 if halls=="Haddington Place" & mi(provider)
replace todrop=1 if halls=="Parker House" & mi(provider)
replace todrop=1 if tag==1 & num==2
duplicates tag postcode, g(dups)
replace todrop=1 if dups==18 & num==1
replace todrop=1 if dups==15 & num==2
replace todrop=1 if dups==8 & num==1
replace todrop=1 if dups==5 & num==2

bysort postcode: replace uni=uni[_n+1] if !mi(uni[_n+1]) & mi(uni)
bysort postcode: replace uni=uni[_n+1] if !mi(uni[_n+1]) & mi(uni)
bysort postcode: replace uni=uni[_n-1] if !mi(uni[_n-1]) & mi(uni)
bysort postcode: replace provider=provider[_n+1] if !mi(provider[_n+1]) & mi(provider)
bysort postcode: replace provider=provider[_n+1] if !mi(provider[_n+1]) & mi(provider)
bysort postcode: replace provider=provider[_n-1] if !mi(provider[_n+1]) & mi(provider)
drop if todrop
drop dups tag sruc
duplicates tag postcode, g(dups)
replace todrop = 1 in 5
replace todrop = 1 in 23
replace todrop = 1 in 33
replace todrop = 1 in 15
replace todrop = 1 in 37
replace todrop = 1 in 41
replace todrop = 1 in 74
replace todrop = 1 in 108
replace postcode = "EH3 9QP" in 111
replace todrop = 1 in 117
replace todrop = 1 in 125
replace todrop = 1 in 130
replace todrop = 1 in 143
replace todrop = 1 in 197
replace todrop = 1 in 206
replace todrop = 1 in 229
replace todrop = 1 in 244
replace todrop = 1 in 277
replace todrop = 1 if halls=="Trinity Court" & postcode=="AB24 5QU"
drop if todrop
drop dups
duplicates tag halls,gen(dups)
replace todrop=1 if num==2 & dups==1
drop if todrop
drop dups todrop
drop city
replace provider=uni if mi(provider)
replace provider = "University of Aberdeen" in 17
replace uni= "University of Aberdeen" in 17
replace provider = "University of Aberdeen" in 18
replace uni= "University of Aberdeen" in 18
replace provider = "Westport Property" in 37
replace provider = "Mears Student Life" in 47
replace provider = "Carling Property Group" in 52
replace provider = "University of Edinburgh" in 63
replace provider = "Edinburgh Student Housing Cooperative" in 64
replace provider = "ELS Students" in 72
replace provider = "Prestige Student Living" in 93
replace halls = "Fountainbridge" in 101
replace provider = "Vita Student" in 101
replace provider = "The Student Housing Company" in 102
replace provider = "Collegiate AC" in 108
replace uni = "University of Edinburgh" in 116
replace provider = "University of Edinburgh" in 116
replace uni = "University of Edinburgh" in 124
replace provider = "University of Edinburgh" in 124
replace provider = "Hello Student" in 125
replace uni = "University of Edinburgh" in 130
replace provider = "University of Edinburgh" in 130
replace provider = "Student Castle" in 138
replace uni = "University of Edinburgh" in 141
replace provider = "University of Edinburgh" in 141
replace provider = "Bastion Property Management" in 144
replace uni = "University of Stirling" in 145
replace provider = "University of Stirling" in 145
replace uni = "University of Stirling" in 148
replace provider = "University of Stirling" in 148
replace uni = "University of Stirling" in 157
replace provider = "University of Stirling" in 157
replace uni = "University of Stirling" in 159
replace provider = "University of Stirling" in 159
replace uni = "University of Stirling" in 160
replace provider = "University of Stirling" in 160
replace uni = "University of Stirling" in 161
replace provider = "University of Stirling" in 161
replace uni = "University of Stirling" in 162
replace provider = "University of Stirling" in 162
replace halls = "West Village" in 175
replace provider = "Downing Students" in 175
replace uni = "University of Glasgow" in 181
replace provider = "University of Glasgow" in 181
replace provider = "Prime Student Living" in 185
replace halls = "Willowbank" in 193
replace provider = "Hello Student" in 193
replace provider = "Kaplan Living" in 202
replace uni = "University of Glasgow" in 203
replace provider = "University of Glasgow" in 203
replace provider = "Nido Student" in 214
replace provider = "Hello Student" in 223
replace provider = "City College of Glasgow" in 226
replace uni = "University of the Highlands and Islands" in 230
replace provider = "University of the Highlands and Islands" in 230
replace provider = "Cragie Campus" in 234
replace halls = "East Shore" in 238
replace provider = "Homes for Students" in 238
replace halls = "Ayton House" in 240
replace provider = "Hello Student" in 240
replace uni = "University of St Andrews" in 249
replace provider = "University of St Andrews" in 249
replace uni = "University of the Highlands and Islands" in 263
replace provider = "University of the Highlands and Islands" in 263
gen todrop=0
replace todrop=1 if mi(provider)
duplicates tag halls,gen(dups)
replace todrop=1 if num==2 & dups==1
drop if todrop
drop todrop dups num
gen privatehalls=mi(uni)
gen unihalls=!mi(uni)
gen numhalls=1
collapse (sum) numhalls ,by(postcode uni)
keep if !mi(uni)
frame copy default halls
compress
replace postcode=subinstr(postcode," ","",.)
drop if postcode==""
sort postcode
levelsof postcode, l(toloop)
local c 0
keep postcode
foreach postcode of local toloop{
frame create postcodes

cap frame postcodes: jsonio rv, file("https://api.postcodes.io/postcodes/`postcode'") elem("result") ob(1)
if !_rc{
frame postcodes: tostring *,replace
frameappend postcodes,drop
}
else{
	frame drop postcodes
}
}
drop postcode
drop if jsonvar1==""
unab toloop: jsonvar*
foreach var of var *{
	local varn: var l `var'
	local varn2 `=subinstr("`varn'","_","",.)'
	local varn3 `=subinstr("`varn2'","/","",.)'
	local varn4 `=subinstr("`varn3'","result","",1)'
	local varn5 `=subinstr("`varn4'","id24","code",1)'
	ren `var' `varn5'
}
ren code* *code
compress
frlink 1:1 postcode,frame(halls)
frget uni numhalls,from(halls)
frlink m:1 uni, frame(startdates)
frget start, from(startdates)
drop startdates halls
gen exclude=0
replace exclude=1 if uni=="SRUC" | uni=="University of the Highlands and Islands"
destring eastings northings,replace
save `datad'/unihalls-clean,replace
