local plots ../plots
local res ../results
local positivec100tick "-400(25)550"
local positivec100lab "-400(100)550"
local positivec100tick22165 "-800(25)800"
local positivec100lab22165 "-800(100)800"
local positivec100tick22172 "-400(25)550"
local positivec100lab22172 "-400(100)550"
local positivec100tick22179 "-400(25)550"
local positivec100lab22179 "-400(100)550"
local positivec100tick22193 "-1500(100)1500"
local positivec100lab22193 "-1500(500)1500"
local positive7daytick "-10(1)30"
local positive7daylab "-10(5)30"
local lnpositivetick "-1.5(.1)1.5"
local lnpositivelab "-1.5(.5)1.5"
local dlnpositivetick "-1(.1)2"
local dlnpositivelab "-1(.5)2"
local ihspositivetick22165 "-5(.5)5"
local ihspositivelab22165 "-5(.5)5"
local ihspositivetick22172 "-1.2(.1)3"
local ihspositivelab22172 "-1.2(.4)3"
local ihspositivetick22179 "-3(.5)3"
local ihspositivelab22179 "-3(.4)3"
local ihspositivetick22193 "-5(.5)5"
local ihspositivelab22193 "-5(.5)5"
local ihspositivetick "-1.2(.1)3"
local ihspositivelab "-1.2(.4)3"
local keeppositive7day 170/500
local keeplnpositive 170/500
local keepdlnpositive 169/497
local keepihspositive 170/500
local 3 mobility
local 2 health
local 1 age
local 4 all
local ihspositive "IHS (Cases)"
local positivec100 "Cases per 100k"
local hetloop 22165 22172 22179 22193
foreach var in ihspositive positivec100{
forv dist=1/5{
forv rhs=0/4{
		if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
use `res'/cases`var'`dist'k`nm'-boot,clear
gen p1=bootp<0.01
gen p5=bootp>=0.01 & bootp<0.05
gen p10=bootp>=0.05 & bootp<0.1
egen ns=rowtotal(p1 p5 p10)
recode ns (0=1) (1=0)
su aic,mean
local aic `=round(`r(mean)',.01)'
two (sc estimate day if p1 , ms(Oh) mc(maroon) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 , ms(Th) mc(forest_green) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 , ms(Sh) mc(navy) sort leg(lab( 3 "* p<0.10"))) ///
(line estimate day ) (line lo day , lc(gs10)) ///
(line hi day , lc(gs10)), yline(0) ///
xlabel(22137(7)22298,angle(45) labs(small)) note("AIC: `aic'") ///
leg(order(1 2 3) r(1)) ylab(``var'lab', angle(45) labs(small)) ymtick(``var'tick',grid) ///
xti("") yti("``var'' relative to `dist'km nearest neighbours", size(small)) scheme(s1mono)

graph export `plots'/nc`dist'km-`var'`nm'-boot.eps, cmyk(on) replace
clear
if "`rhs'"=="0"{
		foreach start of local hetloop{
			frame create temp
			frame temp:use `res'/cases`var'`dist'k`nm'-`start'-boot,clear
			frame temp: gen g=`start'
			frameappend temp,drop
		}
gen p1=bootp<0.01
gen p5=bootp>=0.01 & p<0.05
gen p10=bootp>=0.05 & p<0.1
egen ns=rowtotal(p1 p5 p10)
recode ns (0=1) (1=0)
su aic,mean
local aic `=round(`r(mean)',.01)'
local lp22165 dash_dot
local lc22165 gs5
local lp22172 longdash
local lc22172 navy
local lp22179 solid
local lc22179 maroon
local lp22193 dash
local lc22193 forest_green
		foreach start in 22165 22172 22179 22193{
			su aic if g==`start',mean
local aic `=round(`r(mean)',.01)'
		local startd: di %td `start'
		two (sc estimate day if p1 & g==`start' , ms(Oh) mc(maroon) sort leg(lab( 1 "*** p<0.01"))) ///
		(sc estimate day if p5 & g==`start' , ms(Th) mc(forest_green) sort leg(lab( 2 "** p<0.05"))) ///
		(sc estimate day if p10 & g==`start' , ms(Sh) mc(navy) sort leg(lab( 3 "* p<0.10"))) ///
		(line estimate day if g==`start', lc(`lc`start'') lp(`lp`start'') leg(lab(4 "`startd' Start"))) ///
		(line lo day if g==`start' , lc(gs10)) (line hi day if g==`start', lc(gs10)) ///
		, yline(0) xlabel(22137(7)22298,angle(45) labs(small)) note("AIC: `aic'")  ///
		leg(order(1 2 3 4) r(3)) ylab(``var'lab`start'', angle(45) labs(small)) ymtick(``var'tick`start'',grid) ///
		xti("") yti("``var'' relative to `dist'km nearest neighbours", size(small)) scheme(s1mono)

		graph export `plots'/twfe-unihet-`start'-nc`dist'km-`var'`nm'-boot.eps, cmyk(on) replace
		}
	}
}
}
}


/*
two (sc estimate day if p1 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
(sc estimate day if ns & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(X) sort leg(lab( 4 "insignificant"))) ///
(line estimate day if inrange(day,td(14sep2020), ///
td(18jan2021))) (line min95 day if inrange(day, ///
td(14sep2020),td(18jan2021)), lc(gs10)) ///
(line max95 day if inrange(day,td(14sep2020), ///
td(18jan2021)), lc(gs10)), yline(0) ///
xlabel(22172(7)22298,angle(45)) ///
leg(order(1 2 3 4) r(1)) ymtick(``var'tick',grid) ///
xti("") yti("New cases relative neighbours in `dist'km")
*/
