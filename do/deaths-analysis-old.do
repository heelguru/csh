clear all
cd ~/Downloads/eva/
import delimited using covid-deaths-extra-tables-week-28_TableS8.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk28
frame copy default wk28
import delimited using covid-deaths-data-week-37_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk37
frame copy default wk37
import delimited using covid-deaths-data-week-41_Table11.csv, delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk41
frame copy default wk41
import delimited using covid-deaths-data-week-45_Table11.csv, delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk45
frame copy default wk45
import delimited using covid-deaths-data-week-50_Table11.csv, delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk50
frame copy default wk50
import delimited using covid-deaths-21-data-week-02_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk52
frame copy default wk52
import delimited using covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
ren numberofdeaths cumuldeathwk58
foreach num in 28 37 41 45 50 52{
frlink 1:1 intermediatezone, frame(wk`num')
frget cumuldeathwk`num', from(wk`num')
}
drop v*
replace pop=subinstr(pop,",","",.)
destring pop,replace
drop wk*
gen pop100=population2019based/100000
reshape long cumuldeath@,i(intermediatezonecode nameofintermediatezone localauthority) j(weekno,string)
replace weekno=subinstr(weekno,"wk","",.)
egen t=group(weekno)
egen intzone=group(intermediatezonecode)
xtset intzone t
gen deathdiff=d.cumuldeath
gen deaths100=deathdiff/pop100
gen ihsdeaths=asinh(deathdiff/pop100)
ren intermediatezonecode InterZone
save covid-deaths-publicdata,replace
use halls-treatment-control,clear
merge 1:m InterZone using covid-deaths,gen(mdeath) keep(3)
egen interzone=group(InterZone)
xtset interzone t
foreach var of var deaths100 deathdiff ihsdeaths{
forv dist=1/5{
reghdfe `var' ib2.t##i.treated`dist'k,abs(InterZone)
parmest, saving(deaths`var'`dist'k-publicdata)
}
}
