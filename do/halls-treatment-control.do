local datar ../data/raw
local datad ../data/derived
use `datad'/halls-clean,clear
drop if exclude==1
ren msoacode InterZone
gen treated=1
bysort InterZone: egen minstart=min(start)
gen startuni=uni if start==minstart
collapse (sum) privatehall unihall numhalls (firstnm) longitude latitude region admindistrict admincounty minstart uni, by(InterZone treated)
collapse (sum) numhalls (firstnm) longitude latitude region admindistrict admincounty minstart uni, by(InterZone treated)
merge m:1 InterZone using `datad'/SG_IntermediateZone_Bdry_2011.dta, keep(3)
gen hallid=_n 
frame copy default treatment
use `datad'/halls-clean,clear
drop if exclude==1
destring latitude longitude,replace
gen hallid=_n 
geonear hallid latitude longitude using `datad'/SG_IntermediateZone_Bdry_2011.dta,n(_ID lat_degrees long_degrees) with(5) long
frlink m:1 hallid, frame(treatment)
frget minstart,from(treatment)
drop treatment
gen treatment=0
bysort _ID : gen n=_n
ren km_to__ID disttotreat
forv dist=1/5{
gen control`dist'k=dist<=`dist'	
}
collapse (sum) control* (firstnm) hallid minstart,by(_ID)
forv dist=1/5{
replace control`dist'k=. if control`dist'k==0
replace control`dist'k=0 if !mi(control`dist'k)
}
*drop if mi(control1k) &  mi(control2k) &  mi(control3k) &  mi(control4k) &  mi(control5k)   
merge 1:1 _ID using `datad'/SG_IntermediateZone_Bdry_2011,keep(3)
frameappend treatment,drop
forv dist=1/5{
gen treated`dist'k=cond(treated==1,1, ///
									 cond(control`dist'k==0,0,.))
}
duplicates tag _ID, gen(dups)
drop if dups==1 & mi(treated)
drop dups
save `datad'/halls-treatment-control,replace
