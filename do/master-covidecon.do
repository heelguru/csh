*This dofile links all the dofiles needed to 
*do geocode-halls.do // This do file geocodes private and university halls from postcodes
*do geocode-unihalls.do // This do file geocodes halls solely owned by universities
*do halls-treatment-control.do // this aggregates the halls by neighbourhoods
*do halls-donut-treatment-control.do // this aggregates halls data for donuts
do cases-analysis.do // main analysis including TWFE no stagger start estimates
do cases-analysis-donut.do // Donut analysis
do cases-analysis-unihalls.do // Performs estimation using Huntington-Klein's did wrapper
do cases-plots.do // This dofile takes all estimates and plots them for main analysis
do cases-plots-donuts.do // This dofile takes estimates from donut analysis and plots them
do cases-plots-unihalls.do // This dofile plots Callaway and Sant'Anna estimates