	local deaths100tick "-27(1)12"
	local deaths100lab "-27(5)12"
	local deathdifftick "-1.1(.1).5"
	local deathdifflab "-1(.5).5"
	local ihsdeathstick "-1.4(.1).6"
	local ihsdeathslab "-1.4(.2).6"
	foreach var in deaths100 deathdiff ihsdeaths{
	forv dist=1/5{
	use deaths`var'`dist'k-publicdata,clear
	keep in 10/20
	gen odd=mod(_n,2)
	keep if odd
	gen time=real(substr(parm,1,1))
	gen p1=p<0.01
	gen p5=p>=0.01 & p<0.05
	gen p10=p>=0.05 & p<0.1
	egen ns=rowtotal(p1 p5 p10)
	recode ns (0=1) (1=0)
	two (sc estimate time if p1, ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
	(sc estimate time if p5, ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
	(sc estimate time if p10, ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
	(sc estimate time if ns, ms(X) sort leg(lab( 4 "insignificant"))) ///
	(line estimate time) (line min95 time, lc(gs10)) ///
	(line max95 time, lc(gs10)), yline(0) /// 
	xlabel(1 "March-June" 2 "July August" ///
	3 "September" 4 "October" 5 "November" ///
	6 "December" 7 "January",angle(45)) ylab(``var'lab') ///
	leg(order(1 2 3 4) r(1)) ymtick(``var'tick',grid) ///
	xti("") yti("New cases relative neighbours in `dist'km") 
	graph export nd`dist'km-`var'-publicdata.png,width(1000) replace
	}
	}
