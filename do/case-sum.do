clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
import delimited using `datar'/Datazone2011lookup.csv, delim(",") clear varn(1)
ren iz2011_code InterZone
keep InterZone la_name hb_name
collapse (first) la_name hb_name, by(InterZone)
frame copy default catg
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
gen positivec100=positive7day/pop100
frlink m:1 InterZone, frame(catg)
frget la_name, from(catg)
frget hb_name, from(catg)
