timer on 1
xtreg positivec100 ib22137.day##i.treated1k, fe vce(cl hallid)
boottest ///
{22138.day#1.treated1k} {22139.day#1.treated1k} {22140.day#1.treated1k} ///
,weight(webb) nograph noci
set tracedepth 1
set trace on
local day 22137
forv i=1/165{
	mat wildboot= nullmat(wildboot) \   `++day', r(CI_`i') , r(p_`i')
}
