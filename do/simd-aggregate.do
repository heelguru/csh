clear all
local datar ../data/raw
local datad ../data/derived
import delimited using `datar'/Datazone2011lookup.csv, delim(",") varn(1) clear
keep dz2011_code iz2011_code
ren dz2011_code datazone
ren iz2011_code InterZone
frame copy default catalogue
import delimited using `datar'/simd2020_withinds.csv,delim(",") varn(1) clear
ren ïdata_zone datazone
unab vars: *
foreach v of local vars{
	cap confirm numeric variable `v'
		if _rc {
						replace `v'=subinstr(`v',"*","",.)
						replace `v'=subinstr(`v',"%","",.)
						*replace `v'=string(real(`v'))
						destring `v',replace
						}

}
replace attainment=string(real(attainment))
destring attainment,replace
frlink 1:1 datazone,frame(catalogue)
frget InterZone, from(catalogue)
collapse (mean) simd2020v2* income_rate-broadband [fw=total_pop], by(InterZone)
save `datad'/simd-interzone.dta,replace
