clear all
local datar ../data/raw
local datad ../data/derived
local tab ../tables
use `datad'/mobility-data,clear
keep if inrange(day,td(10aug2020),td(22jan2021))
collapse (mean) transit residential retail, by(InterZone)
tempfile mobility
save `mobility'
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
collapse (sum) positive7day, by(InterZone)
merge m:1 InterZone using `datad'/halls-treatment-control, gen(mcase) keep(3)
save `datad'/halls-covid,replace
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
gen ihspositive=asinh(positive)
gen positivec100=positive7day/pop100
merge 1:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge 1:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone  using `mobility', gen(mmob) keep(1 3)
mvencode numhalls,mv(0)
la var StdAreaKm2 "Neighbourhood size (KM$^2$)"
la var numhalls "Number of halls in Neighbourhood"
la var population2019based "Neighbourhood Population (2019 est.)"
la var ihspositive "IHS(Cumulative Cases)"
la var positivec100 "Cumulative Cases per 100k"
la var cif "Comparative Illness Factor"
la var overcrowded_rate "% of Households that are overcrowded"
la var age50plus "% of Neighbourhood Aged 50+"
la var transit "Avg Transit mobility"
la var residential "Avg Residential mobility"
la var retail "Avg Retail mobility"
la def treated1k 0 "Neighbourhood within 1km of halls" 1 "Student Neighbourhood"
la val treated1k treated1k
replace age50plus=age50plus*100
iebaltab ihspositive positivec100 StdAreaKm2 numhalls population2019based ///
cif overcrowded_rate age50plus transit residential retail, ///
grpv(treated1k) savetex(`tab'/sumstats.tex) rowv replace texdoc con(1) order(1 0) ///
tblnote("Cases are given as cumulative over the period 10aug2020-18jan2021. Mobility is measured as the percentage difference relative to January 2020.")
