clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
use `datad'/mobility-data,clear
gen month=mofd(day)
gen d=1
collapse (mean) transit residential, by(InterZone month)
tempfile mobility
save `mobility'
*Taking in population data from latest release of public deaths & storing in pop frame
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode intermediatezone
frame copy default pop
*Taking in NRS death data as of 17/03/21 & cleaning it.
import excel using "`datar'/NRS - University of Stirling - Mirko Moro - deaths by intermediate zone.xlsx", first cellra("A4:M1221") sheet("Tabulate 1 - Table 1") clear
replace A="S02001236" in 2
drop in 1 
drop in 2
ren A intermediatezone
ren * covid*
ren covidintermediatezone intermediatezone
reshape long covid@,i(intermediatezone) j(date,string)
gen month=monthly(date,"MY")
drop date
format month %tm
replace covid="0" if covid=="."
destring covid,replace
frame copy default covidd
import excel using "`datar'/NRS - University of Stirling - Mirko Moro - deaths by intermediate zone.xlsx", first cellra("A4:M1221") sheet("Tabulate 2 - Table 1") clear
replace A="S02001236" in 2
drop in 1 
drop in 2
ren A intermediatezone
ren * allcause*
ren allcauseintermediatezone intermediatezone
reshape long allcause@,i(intermediatezone) j(date,string)
gen month=monthly(date,"MY")
drop date
format month %tm
replace allcause="0" if allcause=="."
destring allcause,replace
frlink 1:1 intermediatezone month, frame(covidd)
frget covid, from(covidd)
frlink m:1 intermediatezone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
egen t=group(month)
egen intzone=group(intermediatezone)
xtset intzone t
sort intzone t
foreach var of var covid allcause{
	gen `var'd100=`var'/pop100
	gen `var'ihs=asinh(`var'/pop100)
}
ren intermediatezone InterZone
save `datad'/covid-donut-deaths,replace
use `datad'/halls-donut-treatment-control,clear
merge 1:m InterZone using `datad'/covid-donut-deaths,gen(mdeath) keep(3)
egen interzone=group(InterZone)
xtset interzone t
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone month using `mobility', gen(mmob) keep(1 3)
local 3 mobility
local 2 health
local 1 age
local 4 all
local c3 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c1 c.age50plus#i.month
local c4 `c1' `c2' `c3'
levelsof minstart,l(hetloop)
	foreach type in covid allcause {
		foreach bit in ihs{
			forv dist=1/5{
				forv rhs=0/4{
					if "`rhs'"!="0"{
					local nm -controls``rhs''
					}
					else{
					local nm
					}
				reghdfe `type'`bit' ib5.t##i.treated`dist'k `c`rhs'' ,abs(InterZone hallid)
				estat ic
				local aic=r(S)[1,5]
				parmest, frame(temp,replace)
				frame change temp
				gen aic=`aic'
				local aic
				save `res'/deathsdonut`type'`bit'`dist'k`nm',replace
				frame change default
				if "`rhs'"=="0" | "`rhs'"=="4"{
	foreach start of local hetloop{
		reghdfe `type'`bit' ib5.t##i.treated`dist'k `c`rhs'' if minstart==`start',abs(InterZone hallid)
		su `var' if e(sample),mean
		local mean =round(`r(mean)',0.001)
		estat ic
		local aic=round(r(S)[1,5],0.001)
		parmest, frame(temp,replace)
		frame change temp
		gen aic=`aic'
		local aic
		save `res'/deathsdonut`type'`bit'`dist'k`nm'-`start',replace
		frame change default
	}
}
				}
			}
		}
	}