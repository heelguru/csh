clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20211010.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
merge m:1 InterZone using `datad'/halls-treatment-control, gen(mcase) keep(3)
save `datad'/halls-covid,replace
gen month=mofd(day)
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
xtset interzone day
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone day using `datad'/mobility-data, gen(mmob) keep(1 3)
sort interzone day
gen lnpositive=ln(positive)
gen ihspositive=asinh(positive)
gen dlnpositive=d.lnpositive
gen positivec100=positive7day/pop100
format month %tm
local 1 age
local 2 health
local 3 mobility
local 4 all
local c3 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c1 c.age50plus#i.month
local c4 `c1' `c2' `c3'
levelsof minstart,l(hetloop)
*positive7day positivec100
foreach var in ihspositive positivec100{
forv dist=1/5{
forv rhs=0/4{
	if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
reghdfe `var' ib22866.day##i.treated`dist'k `c`rhs'' ,abs(InterZone month hallid) cl(InterZone)
estat ic
local aic=r(S)[1,5]
parmest, frame(temp,replace)
frame change temp
gen aic=`aic'
local aic
frame change default

	if "`rhs'"=="0"{
		foreach start of local hetloop{
		reghdfe `var' ib22866.day##i.treated`dist'k `c`rhs'' if minstart==`start'  ,abs(InterZone month hallid) cl(InterZone)
		estat ic
		local aic=r(S)[1,5]
		parmest, frame(temp,replace)
		frame change temp
		gen aic=`aic'
		local aic
		frame change default
		}
	}
}
}
}

