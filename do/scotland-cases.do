/*
clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
import delimited using `datar'/daily-cumul-cases-140521.csv, delim(",") clear stringc(_all)
keep dailycases date cumulative
gen day=date(date,"YMD")
format day %td
keep day daily cum
destring daily cum,replace
gen cumc100=cumulative/547000m
frame copy default on
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
frlink m:1 InterZone, frame(pop)
frget pop,from(pop)
gen pop100=population2019based/100000
gen ihspositive=asinh(positive)
gen positivec100=positive7day/pop100
gcollapse (sum) ihspositive positivec100 positive7day, by(day)
frlink 1:1 day, frame(on)
frget daily cumulative c100,from(on)
two (line positivec100 day, leg(lab( 1 "Sum of 7 Day rolling sum cases"))) (line c100 day, leg(lab(2 "Official cumulative cases"))) scheme(s1mono)
graph export some.png, width(1000) replace
two (line positive7 day, leg(lab( 1 "Sum of 7 Day rolling sum cases"))) (line dailycases day, leg(lab(2 "Official daily cases")))
graph export daily7.png, width(1000) replace
*/
clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
local plots ../plots

import delimited using `datar'/daily-cumul-cases-140521.csv, delim(",") clear stringc(_all)
keep dailycases date cumulative
gen day=date(date,"YMD")
format day %td
keep day daily cum
destring daily cum,replace
gen cumc100=cumulative/(5470000/100000)
frame copy default on
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
gcollapse (sum)  positive7day, by(day)
gen positivec100=positive/(5470000/100000)
frlink 1:1 day, frame(on)
frget daily cumulative cumc100,from(on)
gen dailyc100=daily/(5470000/100000)
two (line positivec100 day, leg(lab( 1 "7 Day new cases per 100k")) yaxis(1) yti("New cases per 100k",axis(1))) (line dailyc100 day, leg(lab(2 "Cases per 100k")) yaxis(1)) ,scheme(s1mono) xlabel(22137(7)22298,angle(45)) xti("")  xline(22160,lc(red) lp(dash)) xline(22165 ,lc(gs5))xline(22172,lc(gs5)) xline(22172,lc(gs0)) xline(22179,lc(gs5)) xline(22193,lc(gs5)) xline(22181,lc(red)) xline(22195,lc(red))
graph export `plots'/scotland-cases.eps, cmyk(on) replace

* (line cumc100 day, leg(lab(3 "Cumulative Cases per 100k")) yaxis(2) yti("Cummulative cases per 100k",axis(2)) lp(longdash)) 
