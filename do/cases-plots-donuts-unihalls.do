local plots ../plots
local res ../results
local positive7daytick "-10(1)20"
local positive7daylab "-10(5)20"
local ihspositivetick "-3(.5)3"
local ihspositivelab "-3(.5)3"
local 1 mobility
local 2 health
local 3 age
local 4 all
foreach var in positive7day ihspositive{
forv dist=1/5{
forv rhs=0/1{
		if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
use `res'/unihet-casesdonut`var'`dist'k`nm',clear
replace parm=lower(parm)
split parm, parse("_")
ren parm1 g
ren parm2 day
foreach bit in g day{
	replace `bit'=substr(`bit',2,.)
	destring `bit',replace
	format `bit' %td
}
format day %td
gen p1=p<0.01
gen p5=p>=0.01 & p<0.05
gen p10=p>=0.05 & p<0.1
egen ns=rowtotal(p1 p5 p10)
recode ns (0=1) (1=0)
two (sc estimate day if p1 , ms(Oh) mc(maroon) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 , ms(Th) mc(forest_green) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 , ms(Sh) mc(navy) sort leg(lab( 3 "* p<0.10"))) ///
(sc estimate day if ns , ms(X) sort leg(lab( 4 "insignificant"))) ///
(line estimate day if g==22165, lc(gs5) lp(dash_dot) leg(lab(5 "07sep2020 Start"))) ///
(line estimate day if g== 22172 , lc(navy) lp(longdash) leg(lab(6 "14sep2020 Start"))) ///
(line estimate day if g== 22179 , lc(maroon) leg(lab(7 "21sep2020 Start"))) ///
, yline(0) xlabel(22137(7)22298,angle(45) labs(small)) ///
leg(order(1 2 3 4 5 6 7) r(2)) ylab(``var'lab' , angle(45) labs(small)) ymtick(``var'tick',grid) ///
xti("") yti("New cases relative to `dist'km nearest neighbours", size(small)) scheme(s1mono)

graph export `plots'/unihet-donut-nc`dist'km-`var'`nm'.eps, cmyk(on) replace

}
}
}


/*
two (sc estimate day if p1 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
(sc estimate day if ns & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(X) sort leg(lab( 4 "insignificant"))) ///
(line estimate day if inrange(day,td(14sep2020), ///
td(18jan2021))) (line min95 day if inrange(day, ///
td(14sep2020),td(18jan2021)), lc(gs10)) ///
(line max95 day if inrange(day,td(14sep2020), ///
td(18jan2021)), lc(gs10)), yline(0) ///
xlabel(22172(7)22298,angle(45)) ///
leg(order(1 2 3 4) r(1)) ymtick(``var'tick',grid) ///
xti("") yti("New cases relative neighbours in `dist'km")
*/
