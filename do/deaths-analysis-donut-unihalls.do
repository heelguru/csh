clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
use `datad'/mobility-data,clear
gen month=mofd(day)
gen d=1
collapse (mean) transit residential, by(InterZone month)
tempfile mobility
save `mobility'
*Taking in population data from latest release of public deaths & storing in pop frame
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode intermediatezone
frame copy default pop
*Taking in NRS death data as of 17/03/21 & cleaning it.
import excel using "`datar'/NRS - University of Stirling - Mirko Moro - deaths by intermediate zone.xlsx", first cellra("A4:M1221") sheet("Tabulate 1 - Table 1") clear
replace A="S02001236" in 2
drop in 1 
drop in 2
ren A intermediatezone
ren * covid*
ren covidintermediatezone intermediatezone
reshape long covid@,i(intermediatezone) j(date,string)
gen month=monthly(date,"MY")
drop date
format month %tm
replace covid="0" if covid=="."
destring covid,replace
frame copy default covidd
import excel using "`datar'/NRS - University of Stirling - Mirko Moro - deaths by intermediate zone.xlsx", first cellra("A4:M1221") sheet("Tabulate 2 - Table 1") clear
replace A="S02001236" in 2
drop in 1 
drop in 2
ren A intermediatezone
ren * allcause*
ren allcauseintermediatezone intermediatezone
reshape long allcause@,i(intermediatezone) j(date,string)
gen month=monthly(date,"MY")
drop date
format month %tm
replace allcause="0" if allcause=="."
destring allcause,replace
frlink 1:1 intermediatezone month, frame(covidd)
frget covid, from(covidd)
frlink m:1 intermediatezone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
egen t=group(month)
egen intzone=group(intermediatezone)
xtset intzone t
sort intzone t
foreach var of var covid allcause{
	gen `var'd100=`var'/pop100
	gen `var'ihs=asinh(`var'/pop100)
}
ren intermediatezone InterZone
save `datad'/covid-donut-deaths-unihalls,replace
use `datad'/unihalls-donut-treatment-control,clear
merge 1:m InterZone using `datad'/covid-donut-deaths-unihalls,gen(mdeath) keep(3)
egen interzone=group(InterZone)
xtset interzone t
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone month using `mobility', gen(mmob) keep(1 3)
gen minmonth=mofd(minstart)
mvencode minmonth,mv(0) // NB: att_gt assumes that non treated units are in time 0
format month %9.0g
gen interzone2=interzone
local 1 mobility
local 2 health
local 3 age
local 4 all
local c1 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c3 c.age50plus#i.month
local c4 `c1' `c2' `c3'
forv dist=1/5{
frame copy default t`dist'k
frame change t`dist'k
keep if !mi(donuttreat`dist'k)
	foreach type in covid allcause {
		foreach bit in ihs{
			forv rhs=0/1{
			if "`rhs'"!="0"{
			local nm -controls``rhs''
			}
			else{
			local nm
			}
			att_gt `type'`bit' month minmonth `c`rhs'', idname(interzone2)
			parmest, saving(`res'/unihet-deathsdonut`var'`dist'k`nm',replace)
			}
		}
	}
frame change default
}