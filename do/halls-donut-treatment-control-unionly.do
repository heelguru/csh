local datar ../data/raw
local datad ../data/derived
local res ../results
use `datad'/halls-clean,clear
ren msoacode InterZone
gen treated=1
collapse (sum) privatehall unihall numhalls (firstnm) longitude latitude region admindistrict admincounty, by(InterZone treated)
collapse (sum) numhalls (firstnm) longitude latitude region admindistrict admincounty, by(InterZone treated)
merge m:1 InterZone using `datad'/SG_IntermediateZone_Bdry_2011.dta, keep(3)
frame copy default treatment
use `datad'/halls-clean,clear
destring latitude longitude,replace
gen hallid=_n 
geonear hallid latitude longitude using `datad'/SG_IntermediateZone_Bdry_2011.dta,n(_ID lat_degrees long_degrees) with(10) long
ren hallid controlhall
bysort _ID : gen n=_n
ren km_to__ID disttohalls
forv i=1/5{
gen disttohalls`i'k=disttohalls-`i'
gen donuttreat`i'k=cond(abs(disttohalls`i'k)<=`i' & disttohalls`i'k<=0,1, ///
									 cond(abs(disttohalls`i'k)<=`i' & disttohalls`i'k>0,0,.))
}
collapse (sum) donuttreat*,by(_ID)
forv dist=1/5{
replace donuttreat`dist'k=. if donuttreat`dist'k==0
replace donuttreat`dist'k=0 if !mi(donuttreat`dist'k)
}
*drop if mi(control1k) &  mi(control2k) &  mi(control3k) &  mi(control4k) &  mi(control5k)   
merge 1:1 _ID using `datad'/SG_IntermediateZone_Bdry_2011,keep(3)
* Ensure that halls are dropped
frameappend treatment,drop
levelsof _ID if treated==1, l(todrop)
foreach id of local todrop{
	drop if _ID==`id'
}
drop treated
forv dist=1/5{
gen treated`dist'k=cond(donuttreat`dist'k>=1,1, ///
									 cond(donuttreat`dist'k==0,0,.))
}
save `datad'/halls-donut-treatment-control,replace
