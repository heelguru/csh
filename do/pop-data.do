clear all
local datar ../data/raw
local datad ../data/derived
import delimited using `datar'/iz2011-pop-est_01092020.csv,delim(",") clear varn(1)
keep if year==2019
drop if intzoneqf=="d"
keep if sexqf=="d"
forv age=30(10)80{
 egen age`age'plus=rowtotal(age`age'-age90plus)
}
forv a=30(10)90{
	replace age`a'plus=age`a'plus/allages
}
keep intzone age30plus-age80plus age90plus
ren intzone InterZone
save `datad'/pop-data,replace
