clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
merge m:1 InterZone using `datad'/unihalls-treatment-control, gen(mcase) keep(3)
save `datad'/unihalls-covid,replace
gen month=mofd(day)
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
xtset interzone day
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone day using `datad'/mobility-data, gen(mmob) keep(1 3)
sort interzone day
gen lnpositive=ln(positive)
gen ihspositive=asinh(positive)
gen dlnpositive=d.lnpositive
gen positivec100=positive7day/pop100
format month %tm
local 1 mobility
local 2 health
local 3 age
local 4 all
local c1 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c3 c.age50plus#i.month
local c4 `c1' `c2' `c3'
gen interzone2=interzone
mvencode minstart,mv(0) // NB: att_gt assumes that non treated units are in time 0
format day %9.0g
format minstart %9.0g
forv dist=1/5{
frame copy default t`dist'k
frame change t`dist'k
keep if !mi(treated`dist'k)
foreach var in ihspositive positivec100{
forv rhs=0/1{
	if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
att_gt `var' day minstart `c`rhs'', idname(interzone2)
parmest
}
}
frame change default
}
