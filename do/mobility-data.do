local datar ../data/raw
local datad ../data/derived
local res ../resultsclear all
import delimited using `datar'/Datazone2011lookup.csv, delim(",") varn(1) clear
keep dz2011_code iz2011_code la_name
ren dz2011_code datazone
ren iz2011_code InterZone
ren la_name la
collapse (firstnm) datazone, by(InterZone la)
drop datazone
save `datad'/intzone-catalogue,replace
*frame copy default catalogue
import delimited using `datar'/iso31662-scotland.csv,delim(",") clear varn(1)
gen tokeep=1
keep iso_3166_2_code tokeep
frame copy default scotland
import delimited using `datar'/2020_GB_Region_Mobility_Report.csv,delim(",") clear
ren sub_region_1 la
keep date la iso_3166_2_code retail grocery parks transit residential
foreach var in retail grocery parks transit residential{
	ren `var' `var'
}
drop if mi(la)
replace la=subinstr(la," Council","",.)
frlink m:1 iso_3166_2_code,frame(scotland)
frget tokeep, from(scotland)
keep if tokeep==1
drop scotland tokeep
joinby la using `datad'/intzone-catalogue ,unmatched(none)
gen day=date(date,"YMD")
format day %td
drop date
save `datad'/mobility-data,replace
