local plots ../plots
local res ../results
local covidd100tick "-40(1)15"
local covidd100lab "-40(5)15"
local covidihstick "-1.6(.1)1.2"
local covidihslab "-1.6(.2)1.2"
local allcaused100tick "-40(5)40"
local allcaused100lab "-40(20)40"
local allcauseihstick "-.8(.1)1.5"
local allcauseihslab "-.8(.5)1.5"	
local 1 mobility
local 2 health
local 3 age
local 4 all
local hetloop 22165 22172 22179 22193
foreach type in allcause covid{
	foreach bit in d100 ihs{
		forv dist=1/5{
			forv rhs=0/4{
				if "`rhs'"!="0"{
				local nm -controls``rhs''
				}
				else{
				local nm
				}
			use `res'/deathsdonut`type'`bit'`dist'k`nm',clear
			gen tokeep=strpos(parm,"#1.")>0
			keep if tokeep
			gen time=real(subinstr(subinstr(substr(parm,1,2),".","",.),"b","",.))
			gen p1=p<0.01
			gen p5=p>=0.01 & p<0.05
			gen p10=p>=0.05 & p<0.1
			egen ns=rowtotal(p1 p5 p10)
			recode ns (0=1) (1=0)
			su aic,mean
			local aic `=round(`r(mean)',.01)'
			two (sc estimate time if p1, ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
			(sc estimate time if p5, ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
			(sc estimate time if p10, ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
			(sc estimate time if ns, ms(X) sort leg(lab( 4 "insignificant"))) ///
			(line estimate time) (line min95 time, lc(gs10)) ///
			(line max95 time, lc(gs10)), yline(0) /// 
			xlabel(1 "March" 2 "April" 3 "May" 4 "June" 5 "July" ///
			6 "August" 7 "September" 8 "October" 9 "November" ///
			10 "December" 11 "January" 12 "February",angle(45)) ylab(``type'`bit'lab') ///
			leg(order(1 2 3 4) r(1)) ymtick(``type'`bit'tick',grid) note("AIC: `aic'") ///
			xti("") yti("Deaths relative to neighbours in nearest `dist'km, excluding halls", size(small)) 
			graph export `plots'/donut-nd`dist'km-`type'`bit'`nm'.png,width(1000) replace
							clear
					if "`rhs'"=="0" | "`rhs'"=="4" {
						foreach start of local hetloop{
						frame create temp
						frame temp:use `res'/deathsdonut`type'`bit'`dist'k`nm'-`start',clear
						frame temp: gen g=`start'
						frameappend temp,drop
						}
						gen tokeep=strpos(parm,"#1.")>0
				keep if tokeep
				gen time=real(subinstr(subinstr(substr(parm,1,2),".","",.),"b","",.))
				gen p1=p<0.01
				gen p5=p>=0.01 & p<0.05
				gen p10=p>=0.05 & p<0.1
				egen ns=rowtotal(p1 p5 p10)
				recode ns (0=1) (1=0)
				local lp22165 dash_dot
				local lc22165 gs5
				local lp22172 longdash
				local lc22172 navy
				local lp22179 solid
				local lc22179 maroon
				local lp22193 dash
				local lc22193 forest_green
				foreach start in 22165 22172 22179 22193{
				su aic if g==`start',mean
				local aic `=round(`r(mean)',.01)'
				local startd: di %td `start'
				two (sc estimate time if p1 & g==`start', ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
				(sc estimate time if p5 & g==`start', ms(Th) sort leg(lab( 2 "** p<0.05"))) ///
				(sc estimate time if p10 & g==`start', ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
				(line estimate time if g==`start', lc(`lc`start'') lp(`lp`start'') leg(lab(4 "`startd' Start"))) ///
				(line min95 time if g==`start' , lc(gs10)) ///
				(line max95 time if g==`start', lc(gs10)), ///
				xlabel(1 "March" 2 "April" 3 "May" 4 "June" 5 "July" ///
				6 "August" 7 "September" 8 "October" 9 "November" ///
				10 "December" 11 "January" 12 "February",angle(45)) /// 
				leg(order(1 2 3 4) r(3)) note("AIC: `aic'") /// 
				xti("") yti("Excess mortality relative to `dist'km neighbours",size(small)) yline(0)
				graph export `plots'/twfe-unihet-donut-`start'-nd`dist'km-`type'`bit'`nm'.png,width(1000) replace
								*ylab(``type'`bit'lab')  ymtick(``type'`bit'tick',grid) ///
				}
			}
		}
}
}
}
}