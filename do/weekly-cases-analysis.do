clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
local tab ../tables
use `datad'/mobility-data,clear
gen week=wofd(day)
gen d=1
collapse (mean) transit residential, by(InterZone week)
tempfile mobility
save `mobility'
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
gen week=wofd(day)
format week %tw
destring positive7day,replace
mvencode positive7day, mv(0)
collapse (sum) positive7day, by(week intzone)
keep week intzone positive7day
keep if inrange(week,wofd(td(10aug2020)),wofd(td(22jan2021)))
ren intzone InterZone
merge m:1 InterZone using `datad'/halls-treatment-control, gen(mcase) keep(3)
save `datad'/halls-covid-weekly,replace
gen month=mofd(dofw(week))
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
xtset interzone week
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone week using `mobility', gen(mmob) keep(1 3)
sort interzone week
gen lnpositive=ln(positive)
gen ihspositive=asinh(positive)
gen dlnpositive=d.lnpositive
gen positivec100=positive7day/pop100
local positive7day "New COVID-19 Cases"
local ihspositive "IHS(New COVID-19 Cases)"
local positivec100 "COVID-19 Cases per 100k"
format month %tm
encode admind,gen(la)
local 0 none
local 3 mobility
local 2 health
local 1 age
local 4 all
local c3 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c1 c.age50plus#i.month
local c4 `c1' `c2' `c3'
local h0  `c2' `c1'
local outregopts "starlevels(10 5 1) starloc(1) bdec(3) tex nolegend summstat(N) replace varlabels fragment se"
levelsof minstart,l(hetloop)
foreach var in positive7day ihspositive positivec100{
forv dist=1/5{
forv rhs=0/4{
	if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
local cnum=`rhs'+1
		if `cnum'==1 {
			local merge store
			local addtable
		}
		else {
			local merge merge
			local addtable addtable
		}
cap rm `tab'/`var'treated`dist'k.tex
reghdfe `var' i.week##i.treated`dist'k `c`rhs''  ,abs(InterZone month hallid)
su `var' if e(sample),mean
local mean =round(`r(mean)',0.001)
estat ic
local aic=round(r(S)[1,5],0.001)
outreg using `tab'/`var'treated`dist'k.tex, ///
keep(3152.week#1.treated`dist'k 3153.week#1.treated`dist'k ///
3154.week#1.treated`dist'k 3155.week#1.treated`dist'k 3156.week#1.treated`dist'k ///
3157.week#1.treated`dist'k 3158.week#1.treated`dist'k 3159.week#1.treated`dist'k ///
3160.week#1.treated`dist'k 3161.week#1.treated`dist'k 3162.week#1.treated`dist'k ///
3163.week#1.treated`dist'k 3164.week#1.treated`dist'k 3165.week#1.treated`dist'k ///
3166.week#1.treated`dist'k 3167.week#1.treated`dist'k 3168.week#1.treated`dist'k ///
3169.week#1.treated`dist'k 3170.week#1.treated`dist'k 3171.week#1.treated`dist'k ///
3172.week#1.treated`dist'k 3173.week#1.treated`dist'k 3174.week#1.treated`dist'k ///
3175.week#1.treated`dist'k) `outregopts' ///
addrows("Mean Outcome","`mean'" \ "Controls","``rhs''"\"AIC","`=round(`aic',.01)'") ///
rtitles("2020w33"\""\"2020w34"\""\"2020w35"\""\"2020w36"\"" ///
\"2020w37"\""\"2020w38"\""\"2020w39"\""\"2020w40"\"" ///
\"2020w41"\""\"2020w42"\""\"2020w43"\""\"2020w44"\"" ///
\"2020w45"\""\"2020w46"\""\"2020w47"\""\"2020w48"\"" ///
\"2020w49"\""\"2020w50"\""\"2020w51"\""\"2020w52"\"" ///
\"2021w1"\""\"2021w2"\""\"2021w3"\""\"2021w4"\"") ///
ctitles("DV ``var''","(`cnum')") `merge'(`var't`dist'k)

if "`rhs'"=="0"{
foreach start of local hetloop{
		if "`rhs'"=="0" & "`start'"=="22165"  {
			local merge2 store
			local addtable2
		}
		else {
			local merge2 merge
			local addtable2 addtable
		}
	local ctitle : display %td `start'
	cap rm `tab'/het`var'treated`dist'k.tex
reghdfe `var' i.week##i.treated`dist'k `c`rhs'' if minstart==`start',abs(InterZone month hallid)
su `var' if e(sample),mean
local mean =round(`r(mean)',0.001)
estat ic
local aic=round(r(S)[1,5],0.001)
outreg using `tab'/het`var'treated`dist'k.tex, ///
keep(3152.week#1.treated`dist'k 3153.week#1.treated`dist'k ///
3154.week#1.treated`dist'k 3155.week#1.treated`dist'k 3156.week#1.treated`dist'k ///
3157.week#1.treated`dist'k 3158.week#1.treated`dist'k 3159.week#1.treated`dist'k ///
3160.week#1.treated`dist'k 3161.week#1.treated`dist'k 3162.week#1.treated`dist'k ///
3163.week#1.treated`dist'k 3164.week#1.treated`dist'k 3165.week#1.treated`dist'k ///
3166.week#1.treated`dist'k 3167.week#1.treated`dist'k 3168.week#1.treated`dist'k ///
3169.week#1.treated`dist'k 3170.week#1.treated`dist'k 3171.week#1.treated`dist'k ///
3172.week#1.treated`dist'k 3173.week#1.treated`dist'k 3174.week#1.treated`dist'k ///
3175.week#1.treated`dist'k) `outregopts' ///
addrows("Mean Outcome","`mean'" \ "Controls","``rhs''"\"AIC","`=round(`aic',.01)'") ///
rtitles("2020w33"\""\"2020w34"\""\"2020w35"\""\"2020w36"\"" ///
\"2020w37"\""\"2020w38"\""\"2020w39"\""\"2020w40"\"" ///
\"2020w41"\""\"2020w42"\""\"2020w43"\""\"2020w44"\"" ///
\"2020w45"\""\"2020w46"\""\"2020w47"\""\"2020w48"\"" ///
\"2020w49"\""\"2020w50"\""\"2020w51"\""\"2020w52"\"" ///
\"2021w1"\""\"2021w2"\""\"2021w3"\""\"2021w4"\"") ///
ctitles("DV ``var''","`ctitle'") `merge2'(het`var't`dist'k)
}
}
/*
estat ic
local aic=r(S)[1,5]
parmest, frame(temp,replace)
frame change temp
gen aic=`aic'
local aic
save `res'/cases`var'`dist'k`nm',replace
frame change default
*/
}
}
}
cd ../tables/
local toloop: dir . files "*.tex" 
foreach file of local toloop{
!perl -pi -e 's/\\noalign\{\\smallskip\}//g' `file'
!perl -0777 -pi -e 's/hline/toprule/' `file'
!perl -0777 -pi -e 's/hline/midrule/' `file'
!perl -0777 -pi -e 's/hline/bottomrule/' `file'
!perl -pi -e 's/(\d{3})\.00/$1/g' `file'
!perl -pi -e 's/\\(begin|end)\{center\}//g' `file'
!perl -pi -e 's/(\\bottomrule\\end\{tabular\})\\\\/$1/' `file'
}