cd "/home/hector/Dropbox/csh/do"
use ../data/derived/temp1k,clear
rcall clear
*att_gt ihspositive day minstart, idname(interzone)
format minstart %9.0g
format day %9.0g
*att_gt ihspositive day minstart, idname(interzone)
*di "`r(error)'"
att_gt ihspositive day minstart, idname(interzone2)

/*
				// AVOID STATA LIMITS IN TERMS OF NUMBER OF ARGUMENTS	
				// ------------------------------------------------------------
				if wordcount(`"`macval(content)'"') <= 300  {
					matrix define `name' = (`content')
				}
				else {
					local content2:subinstr local content "NA" ".",all
					local content3:subinstr local content2 "," "",all
					tokenize "`content3'"
					while !missing("`1'") {
						local n 0      //RESET
						local content4 //RESET
						while `n' <= 300 {
							local n `++n'
							local content4 `content4', `1'
							macro shift
						}
						// append pieces of the long returned matrix
							matrix `name' = nullmat(`name')  `content4'
						macro shift
					}
				}	
*/
