local datar ../data/raw
local datad ../data/derived
use `datar'/InterZone-RurUrb,clear
keep DataZone UR8FOLD UR6FOLD UR3FOLD UR2FOLD StdAreaKm2
frame copy default rural
import delimited using Datazone2011lookup.csv, delim(",") clear varn(1)
ren dz2011_code DataZone
keep DataZone iz2011_code
ren iz2011_code InterZone
frlink 1:1 DataZone, frame(rural)
frget UR8FOLD UR6FOLD UR3FOLD UR2FOLD StdAreaKm2,from(rural)
collapse (mean) UR8FOLD UR6FOLD UR3FOLD UR2FOLD (sum)StdAreaKm2, by(InterZone)
save `datad'/iz-rural,replace
