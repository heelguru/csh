local plots ../plots
local res ../results
local positive7daytick "-10(1)20"
local positive7daylab "-10(5)20"
local lnpositivetick "-1.5(.1)1.5"
local lnpositivelab "-1.5(.5)1.5"
local dlnpositivetick "-1(.1)2"
local dlnpositivelab "-1(.5)2"
local ihspositivetick "-.6(.1)1.2"
local ihspositivelab "-.6(.2)1.2"
local keeppositive7day 170/500
local keeplnpositive 170/500
local keepdlnpositive 169/497
local keepihspositive 170/500
local 1 mobility
local 2 health
local 3 age
local 4 all
foreach var in positive7day ihspositive{
forv dist=1/5{
forv rhs=0/4{
	if "`rhs'"!="0"{
	local nm -controls``rhs''
	}
	else{
	local nm
	}
use `res'/casesdonut`var'`dist'k`nm'-cbonly,clear
gen tokeep=strpos(parm,"#1.")>0
keep if tokeep
gen day=real(substr(parm,1,5))
format day %td
gen p1=p<0.01
gen p5=p>=0.01 & p<0.05
gen p10=p>=0.05 & p<0.1
egen ns=rowtotal(p1 p5 p10)
recode ns (0=1) (1=0)
su aic,mean
local aic `=round(`r(mean)',.01)'
two (sc estimate day if p1 , ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 , ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 , ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
(sc estimate day if ns , ms(X) sort leg(lab( 4 "insignificant"))) ///
(line estimate day ) (line min95 day , lc(gs10)) ///
(line max95 day , lc(gs10)), yline(0) ///
xlabel(22137(7)22298,angle(45)) note("AIC: `aic'") ///
leg(order(1 2 3 4) r(1)) ylab(``var'lab') ymtick(``var'tick',grid) ///
xti("") yti("New Cases relative to neighbours in nearest `dist'km, excluding halls", size(small))

graph export `plots'/donut-nc`dist'km-`var'`nm'-cbonly.png,width(1000) replace

}
}
}


/*
two (sc estimate day if p1 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Oh) sort leg(lab( 1 "*** p<0.01"))) ///
(sc estimate day if p5 &inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Th) sort leg(lab( 2 "* p<0.05"))) ///
(sc estimate day if p10 & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(Sh) sort leg(lab( 3 "* p<0.10"))) ///
(sc estimate day if ns & inrange(day,td(14sep2020), ///
td(18jan2021)), ms(X) sort leg(lab( 4 "insignificant"))) ///
(line estimate day if inrange(day,td(14sep2020), ///
td(18jan2021))) (line min95 day if inrange(day, ///
td(14sep2020),td(18jan2021)), lc(gs10)) ///
(line max95 day if inrange(day,td(14sep2020), ///
td(18jan2021)), lc(gs10)), yline(0) ///
xlabel(22172(7)22298,angle(45)) ///
leg(order(1 2 3 4) r(1)) ymtick(``var'tick',grid) ///
xti("") yti("New cases relative neighbours in `dist'km")
*/
