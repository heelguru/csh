clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
local plots ../plots
import delimited using `datar'/Datazone2011lookup.csv,delim(",") clear varn(1)
collapse (first) la_name la_code, by(iz2011_code)
ren iz2011_code InterZone
frame copy default lookup
use `datad'/halls-clean,clear
drop if exclude==1
save `datad'/halls-temp,replace
use `datad'/halls-treatment-control,clear
frlink 1:1 InterZone,frame(lookup)
frget la_name la_code,from(lookup)
encode la_name, gen(ad)
gen cb=inlist(ad,4,5,7,8,9,10,12,14,18,20,21,22,23,24)
gen glasgow=inlist(ad,7,9,11,13,15,17)
gen edinburgh=inlist(ad,3,8,12)
gen aberdeen=inlist(ad,1,2)
gen stirling=inlist(ad,4,16)
la def treated5k 1 "Halls Neighbourhood" 0 "Adjacent Neighbourhood within 5km"
la val treated5k treated5k
spmap treated5k using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta, id(_ID) clm(u) ///
cln(2) ocolor(none ..) ndocolor(none ..) legtitle(varlab)  ///
polygon(data(`datad'/scotlandlas_shp)) point(data(`datad'/halls-clean) ///
	x(eastings) y(northings) size(tiny) fc(red))
graph export `plots'/map.eps, cmyk(on) replace
spmap treated5k if la_name=="Dundee City" | la_name=="Fife"  ///
using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta , id(_ID) clm(u) cln(2)  ///
ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) select(keep if _ID==355| _ID==334)) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Dundee City" | admindistrict=="Fife") ///
size(tiny) fc(red))
graph export `plots'/dundee.eps, cmyk(on) replace

spmap treated5k if glasgow using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) ///
select(keep if _ID==357 | _ID==358| _ID==351 | _ID==352 | _ID==331)) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Glasgow City"| admindistrict=="South Lanarkshire" ///
| admindistrict=="East Dunbartonshire" | admindistrict=="East Renfrewshire"| ///
admindistrict=="Renfrewshire"| admindistrict=="West Dunbartonshire") ///
size(tiny) fc(red))
graph export `plots'/glasgow.eps, cmyk(on) replace

spmap treated5k if edinburgh using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) ///
select(keep if  _ID==350| _ID==337  )) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="City of Edinburgh"| admindistrict=="East Lothian" ///
| admindistrict=="Midlothian" | admindistrict=="West Lothian") ///
size(tiny) fc(red))
graph export `plots'/edinburgh.eps, cmyk(on) replace

spmap treated5k if stirling using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Stirling"| admindistrict=="Clackmannanshire") ///
size(tiny) fc(red))
graph export `plots'/stirling.eps, cmyk(on) replace

spmap treated5k if aberdeen using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) select(keep if _ID==347)) point(data(`datad'/halls-temp) ///
	x(eastings) y(northings) select(keep if admindistrict=="Aberdeen City") ///
	size(tiny) fc(red))
graph export `plots'/aberdeen.eps, cmyk(on) replace

use `datad'/halls-donut-treatment-control,clear
frlink 1:1 InterZone,frame(lookup)
frget la_name la_code,from(lookup)
encode la_name, gen(ad)
gen cb=inlist(ad,4,5,7,8,9,10,12,14,18,20,21,22,23,24)
gen glasgow=inlist(ad,8,10,13,15,16,18,20)
gen edinburgh=inlist(ad,4,9,14,21)
replace edinburgh=1 if inlist(_ID,530,531,532,546)
gen dundee=inlist(ad,3,7,12)
replace dundee=0 if inlist(_ID,530,531,532,546)
gen aberdeen=inlist(ad,1,2)
gen stirling=inlist(ad,5,11,19)
la def treated5k 0 "Neighbourhood near halls 0-5km" 1 "Comparison Neighbourhood 5-10km"
la val treated5k treated5k

spmap treated5k using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta, id(_ID) clm(u) cln(2) ocolor(none ..) ndocolor(none ..) legtitle(varlab) polygon(data(`datad'/scotlandlas_shp)) point(data(`datad'/halls-clean) x(eastings) y(northings) size(tiny) fc(red))
graph export `plots'/map-donut.eps, cmyk(on) replace
spmap treated5k if dundee  ///
using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta , id(_ID) clm(u) cln(2)  ///
ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) select(keep if _ID==355| _ID==334)) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Dundee City" | admindistrict=="Fife" | admindistrict=="Angus") ///
size(tiny) fc(red))
graph export `plots'/dundee-donut.eps, cmyk(on) replace

spmap treated5k if glasgow using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) ///
select(keep if _ID==357 | _ID==358| _ID==351 | _ID==352 | _ID==331)) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Glasgow City"| admindistrict=="South Lanarkshire" | admindistrict=="North Lanarkshire" ///
| admindistrict=="East Dunbartonshire" | admindistrict=="East Renfrewshire"| ///
admindistrict=="Renfrewshire"| admindistrict=="West Dunbartonshire") ///
size(tiny) fc(red))
graph export `plots'/glasgow-donut.eps, cmyk(on) replace

spmap treated5k if edinburgh using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) ///
select(keep if  _ID==350| _ID==337  )) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="City of Edinburgh"| admindistrict=="East Lothian" ///
| admindistrict=="Midlothian" | admindistrict=="West Lothian") size(tiny) fc(red)) 
graph export `plots'/edinburgh-donut.eps, replace

spmap treated5k if stirling using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Stirling"| admindistrict=="Clackmannanshire") ///
size(tiny) fc(red))
graph export `plots'/stirling-donut.eps, cmyk(on) replace

spmap treated5k if aberdeen using `datad'/SG_IntermediateZone_Bdry_2011_shp.dta ///
, id(_ID) clm(u) cln(2)  ndocolor(none ..) legtitle(varlab) ///
polygon(data(`datad'/scotlandlas_shp) select(keep if _ID==347)) point(data(`datad'/halls-temp) x(eastings) y(northings) ///
select(keep if admindistrict=="Aberdeen City") size(tiny) fc(red))
graph export `plots'/aberdeen-donut.eps, cmyk(on) replace
