clear all
local datar ../data/raw
local datad ../data/derived
local res ../results
import delimited using `datar'/covid-deaths-21-data-week-06_Table11.csv,delim(",") clear varn(3) rowr(3:1284)
drop if mi(intermediatezone)
keep intermediatezonecode population2019based
replace pop=subinstr(pop,",","",.)
destring pop,replace
ren intermediatezonecode InterZone
frame copy default pop
import delimited `datar'/trend_iz_20210311.csv,delim(",") clear stringc(_all)
replace date=substr(date,1,4)+"/"+substr(date,5,2)+"/"+substr(date,7,2)
gen day=date(date,"YMD")
format day %td
keep day intzone positive7day
destring positive,replace
mvencode positive, mv(0)
*keep if inrange(day,td(13jun2020),td(30nov2020))
keep if inrange(day,td(10aug2020),td(22jan2021))
ren intzone InterZone
merge m:1 InterZone using `datad'/halls-donut-treatment-control, gen(mcase) keep(3)
save `datad'/halls-donut-covid,replace
gen month=mofd(day)
encode InterZone, gen(interzone)
frlink m:1 InterZone, frame(pop)
frget population2019based, from(pop)
gen pop100=population2019based/100000
xtset interzone day
merge m:1 InterZone using `datad'/simd-interzone, keep(3) assert(2 3) nogen
merge m:1 InterZone using `datad'/pop-data,keep(3) assert(2 3) nogen
merge 1:1 InterZone day using `datad'/mobility-data, gen(mmob) keep(1 3)
sort interzone day
gen lnpositive=ln(positive)
gen ihspositive=asinh(positive)
gen dlnpositive=d.lnpositive
gen positivec100=positive7day/pop100
format month %tm
local 3 mobility
local 2 health
local 1 age
local 4 all
local c3 transit
local c2 c.cif#i.month c.overcrowded_rate#i.month 
local c1 c.age50plus#i.month
local c4 `c1' `c2' `c3'
levelsof minstart,l(hetloop)
foreach var in ihspositive positivec100{
forv dist=1/5{
forv rhs=0/4{
	if "`rhs'"!="0"{
		local nm -controls``rhs''
	}
	else{
		local nm
	}
xtreg `var' ib22137.day##i.treated`dist'k `c`rhs'' i.month ,fe cl(InterZone)
estat ic
local aic=r(S)[1,5]
cap noi cap boottest ///
{22139.day#1.treated`dist'k} {22140.day#1.treated`dist'k} ///
{22141.day#1.treated`dist'k} {22142.day#1.treated`dist'k} {22143.day#1.treated`dist'k} ///
{22144.day#1.treated`dist'k} {22145.day#1.treated`dist'k} {22146.day#1.treated`dist'k} ///
{22147.day#1.treated`dist'k} {22148.day#1.treated`dist'k} {22149.day#1.treated`dist'k} ///
{22150.day#1.treated`dist'k} {22151.day#1.treated`dist'k} {22152.day#1.treated`dist'k} ///
{22153.day#1.treated`dist'k} {22154.day#1.treated`dist'k} {22155.day#1.treated`dist'k} ///
{22156.day#1.treated`dist'k} {22157.day#1.treated`dist'k} {22158.day#1.treated`dist'k} ///
{22159.day#1.treated`dist'k} {22160.day#1.treated`dist'k} {22161.day#1.treated`dist'k} ///
{22162.day#1.treated`dist'k} {22163.day#1.treated`dist'k} {22164.day#1.treated`dist'k} ///
{22165.day#1.treated`dist'k} {22166.day#1.treated`dist'k} {22167.day#1.treated`dist'k} ///
{22168.day#1.treated`dist'k} {22169.day#1.treated`dist'k} {22170.day#1.treated`dist'k} ///
{22171.day#1.treated`dist'k} {22172.day#1.treated`dist'k} {22173.day#1.treated`dist'k} ///
{22174.day#1.treated`dist'k} {22175.day#1.treated`dist'k} {22176.day#1.treated`dist'k} ///
{22177.day#1.treated`dist'k} {22178.day#1.treated`dist'k} {22179.day#1.treated`dist'k} ///
{22180.day#1.treated`dist'k} {22181.day#1.treated`dist'k} {22182.day#1.treated`dist'k} ///
{22183.day#1.treated`dist'k} {22184.day#1.treated`dist'k} {22185.day#1.treated`dist'k} ///
{22186.day#1.treated`dist'k} {22187.day#1.treated`dist'k} {22188.day#1.treated`dist'k} ///
{22189.day#1.treated`dist'k} {22190.day#1.treated`dist'k} {22191.day#1.treated`dist'k} ///
{22192.day#1.treated`dist'k} {22193.day#1.treated`dist'k} {22194.day#1.treated`dist'k} ///
{22195.day#1.treated`dist'k} {22196.day#1.treated`dist'k} {22197.day#1.treated`dist'k} ///
{22198.day#1.treated`dist'k} {22199.day#1.treated`dist'k} {22200.day#1.treated`dist'k} ///
{22201.day#1.treated`dist'k} {22202.day#1.treated`dist'k} {22203.day#1.treated`dist'k} ///
{22204.day#1.treated`dist'k} {22205.day#1.treated`dist'k} {22206.day#1.treated`dist'k} ///
{22207.day#1.treated`dist'k} {22208.day#1.treated`dist'k} {22209.day#1.treated`dist'k} ///
{22210.day#1.treated`dist'k} {22211.day#1.treated`dist'k} {22212.day#1.treated`dist'k} ///
{22213.day#1.treated`dist'k} {22214.day#1.treated`dist'k} {22215.day#1.treated`dist'k} ///
{22216.day#1.treated`dist'k} {22217.day#1.treated`dist'k} {22218.day#1.treated`dist'k} ///
{22219.day#1.treated`dist'k} {22220.day#1.treated`dist'k} {22221.day#1.treated`dist'k} ///
{22222.day#1.treated`dist'k} {22223.day#1.treated`dist'k} {22224.day#1.treated`dist'k} ///
{22225.day#1.treated`dist'k} {22226.day#1.treated`dist'k} {22227.day#1.treated`dist'k} ///
{22228.day#1.treated`dist'k} {22229.day#1.treated`dist'k} {22230.day#1.treated`dist'k} ///
{22231.day#1.treated`dist'k} {22232.day#1.treated`dist'k} {22233.day#1.treated`dist'k} ///
{22234.day#1.treated`dist'k} {22235.day#1.treated`dist'k} {22236.day#1.treated`dist'k} ///
{22237.day#1.treated`dist'k} {22238.day#1.treated`dist'k} {22239.day#1.treated`dist'k} ///
{22240.day#1.treated`dist'k} {22241.day#1.treated`dist'k} {22242.day#1.treated`dist'k} ///
{22243.day#1.treated`dist'k} {22244.day#1.treated`dist'k} {22245.day#1.treated`dist'k} ///
{22246.day#1.treated`dist'k} {22247.day#1.treated`dist'k} {22248.day#1.treated`dist'k} ///
{22249.day#1.treated`dist'k} {22250.day#1.treated`dist'k} {22251.day#1.treated`dist'k} ///
{22252.day#1.treated`dist'k} {22253.day#1.treated`dist'k} {22254.day#1.treated`dist'k} ///
{22255.day#1.treated`dist'k} {22256.day#1.treated`dist'k} {22257.day#1.treated`dist'k} ///
{22258.day#1.treated`dist'k} {22259.day#1.treated`dist'k} {22260.day#1.treated`dist'k} ///
{22261.day#1.treated`dist'k} {22262.day#1.treated`dist'k} {22263.day#1.treated`dist'k} ///
{22264.day#1.treated`dist'k} {22265.day#1.treated`dist'k} {22266.day#1.treated`dist'k} ///
{22267.day#1.treated`dist'k} {22268.day#1.treated`dist'k} {22269.day#1.treated`dist'k} ///
{22270.day#1.treated`dist'k} {22271.day#1.treated`dist'k} {22272.day#1.treated`dist'k} ///
{22273.day#1.treated`dist'k} {22274.day#1.treated`dist'k} {22275.day#1.treated`dist'k} ///
{22276.day#1.treated`dist'k} {22277.day#1.treated`dist'k} {22278.day#1.treated`dist'k} ///
{22279.day#1.treated`dist'k} {22280.day#1.treated`dist'k} {22281.day#1.treated`dist'k} ///
{22282.day#1.treated`dist'k} {22283.day#1.treated`dist'k} {22284.day#1.treated`dist'k} ///
{22285.day#1.treated`dist'k} {22286.day#1.treated`dist'k} {22287.day#1.treated`dist'k} ///
{22288.day#1.treated`dist'k} {22289.day#1.treated`dist'k} {22290.day#1.treated`dist'k} ///
{22291.day#1.treated`dist'k} {22292.day#1.treated`dist'k} {22293.day#1.treated`dist'k} ///
{22294.day#1.treated`dist'k} {22295.day#1.treated`dist'k} {22296.day#1.treated`dist'k} ///
{22297.day#1.treated`dist'k} {22298.day#1.treated`dist'k} {22299.day#1.treated`dist'k} ///
{22300.day#1.treated`dist'k} {22301.day#1.treated`dist'k} {22302.day#1.treated`dist'k}, ///
weight(webb) nograph noci
*set tracedepth 1
*set trace on
if _rc==0{
local day 22138
forv i=1/164{
*	mat dir
	mat wildboot=nullmat(wildboot) \ `++day', r(V_`i') , r(p_`i') , r(df_r_`i')
}
mat colnames wildboot= day bootv bootp bootdf
frame create boot
frame boot: svmat wildboot, n(col)
mat drop wildboot
parmest, frame(temp,replace)
frame change temp
gen aic=`aic'
local aic
gen tokeep=strpos(parm,"#1.")>0
keep if tokeep
gen day=real(substr(parm,1,5))
format day %td
frlink 1:1 day,frame(boot)
frget bootv bootp bootdf, from(boot)
gen lo=estimate-(invttail(bootdf,0.025)*sqrt(bootv))
gen hi=estimate+(invttail(bootdf,0.025)*sqrt(bootv))
save `res'/casesdonut`var'`dist'k`nm'-boot,replace
frame change default
frame drop boot
}
	if "`rhs'"=="0"{
		foreach start of local hetloop{
		 xtreg `var' ib22138.day##i.treated`dist'k `c`rhs'' i.month if minstart==`start'  ,fe cl(InterZone)
				estat ic
				local aic=r(S)[1,5]
			cap noi cap	boottest ///
		{22139.day#1.treated`dist'k} {22140.day#1.treated`dist'k} ///
		{22141.day#1.treated`dist'k} {22142.day#1.treated`dist'k} {22143.day#1.treated`dist'k} ///
		{22144.day#1.treated`dist'k} {22145.day#1.treated`dist'k} {22146.day#1.treated`dist'k} ///
		{22147.day#1.treated`dist'k} {22148.day#1.treated`dist'k} {22149.day#1.treated`dist'k} ///
		{22150.day#1.treated`dist'k} {22151.day#1.treated`dist'k} {22152.day#1.treated`dist'k} ///
		{22153.day#1.treated`dist'k} {22154.day#1.treated`dist'k} {22155.day#1.treated`dist'k} ///
		{22156.day#1.treated`dist'k} {22157.day#1.treated`dist'k} {22158.day#1.treated`dist'k} ///
		{22159.day#1.treated`dist'k} {22160.day#1.treated`dist'k} {22161.day#1.treated`dist'k} ///
		{22162.day#1.treated`dist'k} {22163.day#1.treated`dist'k} {22164.day#1.treated`dist'k} ///
		{22165.day#1.treated`dist'k} {22166.day#1.treated`dist'k} {22167.day#1.treated`dist'k} ///
		{22168.day#1.treated`dist'k} {22169.day#1.treated`dist'k} {22170.day#1.treated`dist'k} ///
		{22171.day#1.treated`dist'k} {22172.day#1.treated`dist'k} {22173.day#1.treated`dist'k} ///
		{22174.day#1.treated`dist'k} {22175.day#1.treated`dist'k} {22176.day#1.treated`dist'k} ///
		{22177.day#1.treated`dist'k} {22178.day#1.treated`dist'k} {22179.day#1.treated`dist'k} ///
		{22180.day#1.treated`dist'k} {22181.day#1.treated`dist'k} {22182.day#1.treated`dist'k} ///
		{22183.day#1.treated`dist'k} {22184.day#1.treated`dist'k} {22185.day#1.treated`dist'k} ///
		{22186.day#1.treated`dist'k} {22187.day#1.treated`dist'k} {22188.day#1.treated`dist'k} ///
		{22189.day#1.treated`dist'k} {22190.day#1.treated`dist'k} {22191.day#1.treated`dist'k} ///
		{22192.day#1.treated`dist'k} {22193.day#1.treated`dist'k} {22194.day#1.treated`dist'k} ///
		{22195.day#1.treated`dist'k} {22196.day#1.treated`dist'k} {22197.day#1.treated`dist'k} ///
		{22198.day#1.treated`dist'k} {22199.day#1.treated`dist'k} {22200.day#1.treated`dist'k} ///
		{22201.day#1.treated`dist'k} {22202.day#1.treated`dist'k} {22203.day#1.treated`dist'k} ///
		{22204.day#1.treated`dist'k} {22205.day#1.treated`dist'k} {22206.day#1.treated`dist'k} ///
		{22207.day#1.treated`dist'k} {22208.day#1.treated`dist'k} {22209.day#1.treated`dist'k} ///
		{22210.day#1.treated`dist'k} {22211.day#1.treated`dist'k} {22212.day#1.treated`dist'k} ///
		{22213.day#1.treated`dist'k} {22214.day#1.treated`dist'k} {22215.day#1.treated`dist'k} ///
		{22216.day#1.treated`dist'k} {22217.day#1.treated`dist'k} {22218.day#1.treated`dist'k} ///
		{22219.day#1.treated`dist'k} {22220.day#1.treated`dist'k} {22221.day#1.treated`dist'k} ///
		{22222.day#1.treated`dist'k} {22223.day#1.treated`dist'k} {22224.day#1.treated`dist'k} ///
		{22225.day#1.treated`dist'k} {22226.day#1.treated`dist'k} {22227.day#1.treated`dist'k} ///
		{22228.day#1.treated`dist'k} {22229.day#1.treated`dist'k} {22230.day#1.treated`dist'k} ///
		{22231.day#1.treated`dist'k} {22232.day#1.treated`dist'k} {22233.day#1.treated`dist'k} ///
		{22234.day#1.treated`dist'k} {22235.day#1.treated`dist'k} {22236.day#1.treated`dist'k} ///
		{22237.day#1.treated`dist'k} {22238.day#1.treated`dist'k} {22239.day#1.treated`dist'k} ///
		{22240.day#1.treated`dist'k} {22241.day#1.treated`dist'k} {22242.day#1.treated`dist'k} ///
		{22243.day#1.treated`dist'k} {22244.day#1.treated`dist'k} {22245.day#1.treated`dist'k} ///
		{22246.day#1.treated`dist'k} {22247.day#1.treated`dist'k} {22248.day#1.treated`dist'k} ///
		{22249.day#1.treated`dist'k} {22250.day#1.treated`dist'k} {22251.day#1.treated`dist'k} ///
		{22252.day#1.treated`dist'k} {22253.day#1.treated`dist'k} {22254.day#1.treated`dist'k} ///
		{22255.day#1.treated`dist'k} {22256.day#1.treated`dist'k} {22257.day#1.treated`dist'k} ///
		{22258.day#1.treated`dist'k} {22259.day#1.treated`dist'k} {22260.day#1.treated`dist'k} ///
		{22261.day#1.treated`dist'k} {22262.day#1.treated`dist'k} {22263.day#1.treated`dist'k} ///
		{22264.day#1.treated`dist'k} {22265.day#1.treated`dist'k} {22266.day#1.treated`dist'k} ///
		{22267.day#1.treated`dist'k} {22268.day#1.treated`dist'k} {22269.day#1.treated`dist'k} ///
		{22270.day#1.treated`dist'k} {22271.day#1.treated`dist'k} {22272.day#1.treated`dist'k} ///
		{22273.day#1.treated`dist'k} {22274.day#1.treated`dist'k} {22275.day#1.treated`dist'k} ///
		{22276.day#1.treated`dist'k} {22277.day#1.treated`dist'k} {22278.day#1.treated`dist'k} ///
		{22279.day#1.treated`dist'k} {22280.day#1.treated`dist'k} {22281.day#1.treated`dist'k} ///
		{22282.day#1.treated`dist'k} {22283.day#1.treated`dist'k} {22284.day#1.treated`dist'k} ///
		{22285.day#1.treated`dist'k} {22286.day#1.treated`dist'k} {22287.day#1.treated`dist'k} ///
		{22288.day#1.treated`dist'k} {22289.day#1.treated`dist'k} {22290.day#1.treated`dist'k} ///
		{22291.day#1.treated`dist'k} {22292.day#1.treated`dist'k} {22293.day#1.treated`dist'k} ///
		{22294.day#1.treated`dist'k} {22295.day#1.treated`dist'k} {22296.day#1.treated`dist'k} ///
		{22297.day#1.treated`dist'k} {22298.day#1.treated`dist'k} {22299.day#1.treated`dist'k} ///
		{22300.day#1.treated`dist'k} {22301.day#1.treated`dist'k} {22302.day#1.treated`dist'k}, ///
		weight(webb) nograph noci
		if _rc==0{
		local day 22138
		forv i=1/164{
			mat wildboot=nullmat(wildboot) \ `++day', r(V_`i') , r(p_`i') , r(df_r_`i')
		}
		mat colnames wildboot= day bootv bootp bootdf
		frame create boot
		frame boot: svmat wildboot, n(col)
		mat drop wildboot
		parmest, frame(temp,replace)
		frame change temp
		gen aic=`aic'
		local aic
		gen tokeep=strpos(parm,"#1.")>0
		keep if tokeep
		gen day=real(substr(parm,1,5))
		format day %td
		frlink 1:1 day,frame(boot)
		frget bootv bootp bootdf, from(boot)
		frame drop boot
		gen lo=estimate-(invttail(bootdf,0.025)*sqrt(bootv))
		gen hi=estimate+(invttail(bootdf,0.025)*sqrt(bootv))
				save `res'/casesdonut`var'`dist'k`nm'-`start'-boot,replace
				frame change default
				}
		}
	}	
}
}
}
}
